const express = require('express')
const appartmentRoutes = require('./src/routes/appartment.routes')
const authenticationRoutes = require('./src/routes/authentication.routes')
const logger = require('./src/config/appconfig').logger

const app = express()
const port = process.env.PORT || 3000

app.use(express.json())

// Generic endpoint handler - voor alle routes
app.all('*', (req, res, next) => {
  // logger.info('Generieke afhandeling aangeroepen!')
  // ES16 deconstructuring
  const { method, url } = req
  logger.info(`${method} ${url}`)
  next()
})

// Hier installeren we de routes
app.use('/api', authenticationRoutes)
app.use('/api/movies', movieRoutes)

// Handle endpoint not found.
app.all('*', (req, res, next) => {
  logger.error('Endpoint not found.')
  const errorObject = {
    message: 'Endpoint does not exist!',
    code: 404,
    date: new Date()
  }
  next(errorObject)
})

// Error handler
app.use((error, req, res, next) => {
  logger.error('Error handler: ', error.message.toString())
  res.status(error.code).json(error)
})

app.listen(port, () => logger.info(`Example app listening on port ${port}!`))

module.exports = app
